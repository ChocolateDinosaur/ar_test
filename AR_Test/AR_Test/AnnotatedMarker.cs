﻿using Xamarin.Forms;

namespace AR_Test
{
    public class AnnotatedMarker : Label
    {
        public RunnerLocation Location { get; set; }

        public AnnotatedMarker()
        {
            BackgroundColor = Color.White;
        }
    }
}
