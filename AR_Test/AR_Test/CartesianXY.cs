﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AR_Test
{
    public class CartesianXY
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
