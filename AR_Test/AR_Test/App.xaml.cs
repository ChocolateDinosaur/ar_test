﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AR_Test
{
	public partial class App : Application
	{
        public static List<AnnotatedMarker> AllPointerLocations;

        public App ()
		{
			InitializeComponent();

            AllPointerLocations = new List<AnnotatedMarker>();
            MainPage = new CameraPage();
            blahblah();
		}

        public async Task blahblah()
        {
            var loc1 = LonLat2Cartesian(-2.650956, 50.940890);
            var loc2 = LonLat2Cartesian(-2.649904, 50.941566);

            var lal1 = loc1.X.ToString() + " - " + loc1.Y.ToString();
            var lal2 = loc2.X.ToString() + " - " + loc2.Y.ToString();
        }

        public void AddPointerLocation(string title, RunnerLocation location)
        {
            var Annotation = new AnnotatedMarker
            {
                Location = location
            };
            Annotation.Text = title;
        }

        public CartesianXY LonLat2Cartesian(double lon, double lat)
        {
            CartesianXY XY = new CartesianXY();

            var R = 6371000;
            XY.X = Convert.ToInt32( R * Math.Cos(lat) * Math.Cos(lon) );
            XY.Y = Convert.ToInt32( R * Math.Cos(lat) * Math.Sin(lon) );
            return XY;
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
