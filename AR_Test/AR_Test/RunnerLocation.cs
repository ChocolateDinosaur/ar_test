﻿namespace AR_Test
{
    public struct RunnerLocation
    {
        public double Longitude;
        public double Latitude;
        public double Altitude;

        public RunnerLocation(double longitude, double latitude, double altitude = 0)
        {
            Longitude = longitude;
            Latitude = latitude;
            Altitude = altitude;
        }
    }
}
