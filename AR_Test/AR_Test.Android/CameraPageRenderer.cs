﻿using System;
using Android.Views;
using Xamarin.Forms;
using AR_Test;
using AR_Test.Droid;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Android.Hardware;
using Android.Graphics;
using Android.Widget;
using System.IO;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;

[assembly: ExportRenderer(typeof(CameraPage), typeof(CameraPageRenderer))]
namespace AR_Test.Droid
{
    public class CameraPageRenderer : PageRenderer, TextureView.ISurfaceTextureListener
    {
        Android.Hardware.Camera camera;
        global::Android.Views.View view;

        public TextView GyroXLabel;
        public TextView GyroYLabel;
        public TextView GyroZLabel;

        public TextView HeadingLabel;

        Activity activity;
        CameraFacing cameraType;
        TextureView textureView;
        SurfaceTexture surfaceTexture;

        public float FieldOfView
        {
            get
            {
                var p = camera.GetParameters();
                return p.HorizontalViewAngle;
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
            {
                base.OnElementChanged(e);

                if (e.OldElement != null || Element == null)
                {
                    return;
                }

                try
                {
                    SetupUserInterface();
                    AddView(view);
                    SetUpGyro();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
                }
            }

        public void SetUpGyro()
        {
            GyroXLabel = view.FindViewById<TextView>(Resource.Id.GyroX);
            GyroYLabel = view.FindViewById<TextView>(Resource.Id.GyroY);
            GyroZLabel = view.FindViewById<TextView>(Resource.Id.GyroZ);
            HeadingLabel = view.FindViewById<TextView>(Resource.Id.HeadingInfo);

            CrossDeviceMotion.Current.SensorValueChanged += (s, a) =>
            {
                switch (a.SensorType)
                {
                    case MotionSensorType.Accelerometer:
                        var X = ((MotionVector)a.Value).X.ToString();
                        var Y = ((MotionVector)a.Value).Y.ToString();
                        var Z = ((MotionVector)a.Value).Z.ToString();
                        
                        GyroXLabel.Rotation = (float.Parse(X) * 6);
                        GyroYLabel.Rotation = (float.Parse(X) * 6);
                        GyroZLabel.Rotation = (float.Parse(X) * 6);
                        HeadingLabel.Rotation = (float.Parse(X) * 6);

                        DisplayAccelerometerInfo(X,Y,Z);
                        break;
                    case MotionSensorType.Compass:
                        DisplayHeading(a.Value.ToString());
                        break;
                }
            };

            CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer);
            CrossDeviceMotion.Current.Start(MotionSensorType.Compass);
        }

        public void DisplayAccelerometerInfo(string Acceleromtere_X, string Acceleromtere_Y, string Acceleromtere_Z )
        {
            GyroXLabel.Text = Acceleromtere_X;
            GyroYLabel.Text = Acceleromtere_Y;
            GyroZLabel.Text = Acceleromtere_Z;
        }

        public void DisplayHeading(string Heading)
        {
            HeadingLabel.Text = Heading;
        }

        void SetupUserInterface()
            {
                activity = this.Context as Activity;
                view = activity.LayoutInflater.Inflate(Resource.Layout.CameraLayout, this, false);
                cameraType = CameraFacing.Back;

                textureView = view.FindViewById<TextureView>(Resource.Id.textureView);
                textureView.SurfaceTextureListener = this;
            }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            var msw = MeasureSpec.MakeMeasureSpec(r - l, MeasureSpecMode.Exactly);
            var msh = MeasureSpec.MakeMeasureSpec(b - t, MeasureSpecMode.Exactly);

            view.Measure(msw, msh);
            view.Layout(0, 0, r - l, b - t);
        }

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {

        }

        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
        {
            camera = global::Android.Hardware.Camera.Open((int)cameraType);
            textureView.LayoutParameters = new FrameLayout.LayoutParams(width, height);
            surfaceTexture = surface;

            camera.SetPreviewTexture(surface);
            PrepareAndStartCamera();
        }

        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            camera.StopPreview();
            camera.Release();
            return true;
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
        {
            PrepareAndStartCamera();
        }

        void PrepareAndStartCamera()
        {
            camera.StopPreview();

            var display = activity.WindowManager.DefaultDisplay;
            if (display.Rotation == SurfaceOrientation.Rotation0)
            {
                camera.SetDisplayOrientation(90);
            }

            if (display.Rotation == SurfaceOrientation.Rotation270)
            {
                camera.SetDisplayOrientation(180);
            }

            camera.StartPreview();
        }
    }
}