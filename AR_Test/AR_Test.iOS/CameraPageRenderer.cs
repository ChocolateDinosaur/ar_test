﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using AR_Test;
using AR_Test.iOS;
using AVFoundation;
using Foundation;
using UIKit;
using CoreGraphics;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;

[assembly: ExportRenderer(typeof(CameraPage), typeof(CameraPageRenderer))]
namespace AR_Test.iOS
{
    public class CameraPageRenderer : PageRenderer
    {
        UILabel GyroXLabel;
        UILabel GyroYLabel;
        UILabel GyroZLabel;

        UILabel HeadingLabel;

        AVCaptureSession captureSession;
        AVCaptureDeviceInput captureDeviceInput;
        AVCaptureStillImageOutput stillImageOutput;
        UIView liveCameraStream;

        public float FieldOfView
        {
            get
            {
                return captureDeviceInput.Device.ActiveFormat.VideoFieldOfView;
            }
        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                SetupUserInterface();
                SetupLiveCameraStream();
                AuthorizeCameraUse();
                SetUpGyro();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
            }
        }
        
        void ToggleFrontBackCamera()
        {
            var devicePosition = captureDeviceInput.Device.Position;
            if (devicePosition == AVCaptureDevicePosition.Front)
            {
                devicePosition = AVCaptureDevicePosition.Back;
            }
            else
            {
                devicePosition = AVCaptureDevicePosition.Front;
            }

            var device = GetCameraForOrientation(devicePosition);
            ConfigureCameraForDevice(device);

            captureSession.BeginConfiguration();
            captureSession.RemoveInput(captureDeviceInput);
            captureDeviceInput = AVCaptureDeviceInput.FromDevice(device);
            captureSession.AddInput(captureDeviceInput);
            captureSession.CommitConfiguration();
        }
        
        AVCaptureDevice GetCameraForOrientation(AVCaptureDevicePosition orientation)
        {
            var devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);

            foreach (var device in devices)
            {
                if (device.Position == orientation)
                {
                    return device;
                }
            }
            return null;
        }

        public void SetUpGyro()
        {
            CrossDeviceMotion.Current.SensorValueChanged += (s, a) =>
            {
                switch (a.SensorType)
                {
                    case MotionSensorType.Accelerometer:
                        var X = ((MotionVector)a.Value).X.ToString();
                        var Y = ((MotionVector)a.Value).Y.ToString();
                        var Z = ((MotionVector)a.Value).Z.ToString();

                        GyroXLabel.Transform = CGAffineTransform.MakeRotation(0 - float.Parse(X));
                        GyroYLabel.Transform = CGAffineTransform.MakeRotation(0 - float.Parse(X));
                        GyroZLabel.Transform = CGAffineTransform.MakeRotation(0 - float.Parse(X));
                        HeadingLabel.Transform = CGAffineTransform.MakeRotation(0 - float.Parse(X));

                        DisplayAccelerometerInfo(X, Y, Z);
                        break;
                    case MotionSensorType.Compass:
                        DisplayHeading(a.Value.ToString());
                        break;
                }
            };

            CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer);
            CrossDeviceMotion.Current.Start(MotionSensorType.Compass);
        }

        public void DisplayAccelerometerInfo(string Acceleromtere_X, string Acceleromtere_Y, string Acceleromtere_Z)
        {
            GyroXLabel.Text = Acceleromtere_X;
            GyroYLabel.Text = Acceleromtere_Y;
            GyroZLabel.Text = Acceleromtere_Z;
        }

        public void DisplayHeading(string Heading)
        {
            HeadingLabel.Text = Heading;
        }

        void SetupLiveCameraStream()
        {
            captureSession = new AVCaptureSession();

            var viewLayer = liveCameraStream.Layer;
            var videoPreviewLayer = new AVCaptureVideoPreviewLayer(captureSession)
            {
                Frame = liveCameraStream.Bounds
            };
            liveCameraStream.Layer.AddSublayer(videoPreviewLayer);

            var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
            ConfigureCameraForDevice(captureDevice);
            captureDeviceInput = AVCaptureDeviceInput.FromDevice(captureDevice);

            var dictionary = new NSMutableDictionary();
            dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
            stillImageOutput = new AVCaptureStillImageOutput()
            {
                OutputSettings = new NSDictionary()
            };

            captureSession.AddOutput(stillImageOutput);
            captureSession.AddInput(captureDeviceInput);
            captureSession.StartRunning();
        }
        
		void SetupUserInterface()
		{
			var centerButtonX = View.Bounds.GetMidX() - 35f;

			var topLeftX = View.Bounds.X + 25;
			var topRightX = View.Bounds.Right - 65;
            

            var bottomButtonY = View.Bounds.Bottom - 150;
			var topButtonY = View.Bounds.Top + 15;


			var buttonWidth = 70;
			var buttonHeight = 70;

            GyroXLabel = new UILabel()
            {
                Text = "1",
                Frame = new CGRect(topLeftX, topButtonY, buttonWidth, buttonHeight)
            };
            GyroYLabel = new UILabel()
            {
                Text = "2",
                Frame = new CGRect(topRightX, topButtonY, buttonWidth, buttonHeight)
            };
            GyroZLabel = new UILabel()
            {
                Text = "3",
                Frame = new CGRect(topLeftX, bottomButtonY, buttonWidth, buttonHeight)
            };
            HeadingLabel = new UILabel()
            {
                Text = "4",
                Frame = new CGRect(topRightX, bottomButtonY, buttonWidth, buttonHeight)
            };

            liveCameraStream = new UIView()
			{
				Frame = new CGRect(0f, 0f, 320f, View.Bounds.Height)
			};

            View.Add(liveCameraStream);

            View.Add(GyroXLabel);
            View.Add(GyroYLabel);
            View.Add(GyroZLabel);
            View.Add(HeadingLabel);
		}

        void ConfigureCameraForDevice(AVCaptureDevice device)
        {
            var error = new NSError();
            if (device.IsFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus))
            {
                device.LockForConfiguration(out error);
                device.FocusMode = AVCaptureFocusMode.ContinuousAutoFocus;
                device.UnlockForConfiguration();
            }
            else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
            {
                device.LockForConfiguration(out error);
                device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
                device.UnlockForConfiguration();
            }
            else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
            {
                device.LockForConfiguration(out error);
                device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
                device.UnlockForConfiguration();
            }
        }

        async void AuthorizeCameraUse()
        {
            var authorizationStatus = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);
            if (authorizationStatus != AVAuthorizationStatus.Authorized)
            {
                await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
            }
        }
    }
}